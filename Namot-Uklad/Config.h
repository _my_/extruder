/*
 * "Wind&Lay" (Winding and Laying of filament)
 * Copyright (C) 2018, Vitaliy Sapyanov, vynt-1@yandex.ru 
 * 
 * This program is not free software: you can modify for personale use
 * 
 */

//Pin configuration file for Arduino Nano

//#define USE_EEPROM //будут использованы настройки из EEPROM
//#define INVERSE_PARK //парковка укладчика в максимум (по умолчанию в минимум)

//намоточник
// Pin 0 & 1 - tx & rx
#define En_ON           LOW//драйвер включается "нулём"

#define Motor1_EN       2
#define Motor1_DIR      3
#define Motor1_STEP     4
#define Motor1_END      5//"железный" датчик оборотов (исключает Motor1_STEPSD)
//укладчик
#define Motor2_EN       6
#define Motor2_DIR      7
#define Motor2_STEP     8
#define Motor2_END_Min  9//левый концевик
#define Motor2_END_Max  10//правый концевик
//тумблер "старт/стоп"
#define StartStopPin    11
//датчики провиса/натяжения филамента
#define PinLed          13//индикация режима
#define FilaMinPin      19//A5
#define FilaMaxPin      18//A4
#define Motor1_GetSpeed A0//потенциометр задающий скорость Motor1
//-----------------------------------------------------------------
//пруток
#define FILA_DIAMETR         1.75
//намоточник
#define Motor1_STEPpMM  100//мкш на мм
#define Motor1_DefSpeed 200//скорость мм/сек (базавая)
#define Motor1_MinSpeed 50 
#define Motor1_MaxSpeed 300//
#define Motor1_Accel    700//ускорение
//#define Motor1_STEPSD  100
#define Motor1_STEPSD   (16 * 360 / 1.8)//микрошагов на полный оборот (мкш*град/угол)
//укладчик
#define Motor2_STEPpMM  100//мкш на мм
#define Motor2_DefSpeed 200//скорость мм/сек (базавая)
#define Motor2_MinSpeed 50//
#define Motor2_MaxSpeed Motor1_MaxSpeed*2//
#define Motor2_Accel    700//ускорение
#define Motor2_STEPSD   (FILA_DIAMETR * Motor2_STEPpMM)//микрошагов на диаметр прутка
//-----------------------------------------------------------------
#define USE_PARK      //
//#define INVERSE_PARK  //
#define USE_SERIAL    //
#ifdef USE_SERIAL
 #undef SERIAL_BUFFER_SIZE
 #define SERIAL_BUFFER_SIZE 128
#endif
