/*
 * "Wind&Lay" (Winding and Laying of filament)
 * Copyright (C) 2018, Vitaliy Sapyanov, vynt-1@yandex.ru 
 * 
 * This program is not free software: you can modify for personale use
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * depends:
 *https://www.arduinolibraries.info/libraries/accel-stepper
 *
 *v.0.1b - started betta
 *v.0.2b - fix MotorX_EN,
 *v.0.3b -
 */

//#include "D:\Clouds\MEGA\Namot-Uklad\config.h"
#include "config.h"

#include <AccelStepper.h> //https://github.com/waspinator/AccelStepper
AccelStepper Motor1(AccelStepper::DRIVER, Motor1_STEP, Motor1_DIR);// НАМОТОЧНИК
AccelStepper Motor2(AccelStepper::DRIVER, Motor2_STEP, Motor2_DIR);// УКЛАДЧИК

bool PrevStartStop;
bool PrevMotor1_End;
bool Motor1_Stoped;
int Motor1_Speed;
long PrevMillis;
long Motor1_CurPos;
long Motor1_CurSpeed;
float Motor1_Kf = 1.0;

void setup() {
  pinMode(PinLed, OUTPUT);
  digitalWrite(PinLed, HIGH);
  pinMode(Motor1_STEP, OUTPUT);    
  pinMode(Motor1_DIR, OUTPUT);
  pinMode(Motor1_EN, OUTPUT);
  digitalWrite(Motor1_STEP, LOW);
  digitalWrite(Motor1_DIR, LOW);
  digitalWrite(Motor1_EN, !En_ON);
  pinMode(Motor2_STEP, OUTPUT);    
  pinMode(Motor2_DIR, OUTPUT);
  pinMode(Motor2_EN, OUTPUT);
  digitalWrite(Motor2_STEP, LOW);
  digitalWrite(Motor2_DIR, LOW);
  digitalWrite(Motor2_EN, !En_ON);
  #ifdef Motor1_END
  pinMode(Motor1_END, INPUT);
  digitalWrite(Motor1_END, HIGH);
  #endif
  pinMode(FilaMinPin,INPUT);
  pinMode(FilaMaxPin,INPUT);
  digitalWrite(FilaMinPin, HIGH);
  digitalWrite(FilaMaxPin, HIGH);
  //pinMode(Motor1_END_Min, INPUT);
  //pinMode(Motor1_END_Max, INPUT);
  //digitalWrite(Motor1_END_Min, HIGH);
  //digitalWrite(Motor1_END_Max, HIGH);

  pinMode(Motor2_END_Min, INPUT);
  pinMode(Motor2_END_Max, INPUT);
  digitalWrite(Motor2_END_Min, HIGH);
  digitalWrite(Motor2_END_Max, HIGH);
  pinMode(StartStopPin, INPUT);
  digitalWrite(StartStopPin, HIGH);
  pinMode(Motor1_GetSpeed, INPUT);
  Motor1_Speed=analogRead(Motor1_GetSpeed);
  Motor1_Stoped=false;
  PrevStartStop=true;
  PrevMotor1_End=digitalRead(Motor1_END);
  //-----------------------------------------------------------------
  //Motor1.setPinsInverted(bool directionInvert = false, bool stepInvert = false, bool enableInvert = false);
  Motor1.setSpeed(Motor1_DefSpeed);
  Motor1.setMaxSpeed(Motor1_MaxSpeed);
  Motor1.setAcceleration(Motor1_Accel);
  //Motor2.setPinsInverted(bool directionInvert = false, bool stepInvert = false, bool enableInvert = false);
  Motor2.setSpeed(Motor2_DefSpeed);
  Motor2.setMaxSpeed(Motor2_MaxSpeed);
  Motor2.setAcceleration(Motor2_Accel);
  //-----------------------------------------------------------------
  Serial.begin(115200);
  Serial.println("define init done");
  Serial.println("read from eeprom...");
  //чтение и проверка контрольной сумму
  //если всё ок - загрузка настроек
  //Serial.println("read eeprom fault!");
  //передача настроек в формате g-code
}

void OutLed(int Wait) {
  if((millis()-PrevMillis)>Wait)
  {
    PrevMillis=millis();
    digitalWrite(PinLed,!digitalRead(PinLed));
  };
}

void loop() {
  // вкл/выкл укладчика и намотчика (0 - замкнут на землю и выключен)
  if(digitalRead(StartStopPin)==1)
  {
    OutLed(250);//4Hz
    PrevStartStop=true;//закоментировать, чтобы укладчик поркавался только при подаче питания
    digitalWrite(Motor1_EN, En_ON);
    digitalWrite(Motor2_EN, En_ON);
    Motor1_Speed=analogRead(Motor1_GetSpeed);//скорость от потенциометра
    //Serial.print("Analog Speed : "); Serial.println(Motor1_Speed);// если аналоговый сигнал изменяется не в диапозоне 0...1023, мониторинг порта поможет узнать необходимый диапозон
    Motor1_Speed=map(Motor1_Speed, 0,1023, Motor1_MinSpeed,Motor1_MaxSpeed);
    Motor1_Speed=Motor1_Speed*Motor1_Kf;//скорость от потенциометра * коэфициент провиса/натяжения
    //if(abs(Motor1.speed()-Motor1_Speed)>5)//сглаживаем "дребезг" потенциометра
      Motor1.setSpeed(Motor1_Speed);
    Motor1.runSpeed();//эта функция даёт бесконечное вращение, дистанция не требуется, но параметр ускорение не используется  
    //-----------------------------------------------------------------
    //setCurrentPosition(-1) и setCurrentPosition(+1) - хитрый способ изменить направление без доп.переменных
    if(digitalRead(Motor2_END_Min)==0) { Motor2.stop(); Motor2.setCurrentPosition(+1); };//добрались до минимума
    if(digitalRead(Motor2_END_Max)==0) { Motor2.stop(); Motor2.setCurrentPosition(-1); };//добрались до максимума
    //-----------------------------------------------------------------
    #ifdef Motor1_END //используется "железный" датчик оборотов
     if((digitalRead(Motor1_END)==1)&&(!PrevMotor1_End)) Motor1_Stoped=true;//срабатывает по фронту 0->1
     PrevMotor1_End=digitalRead(Motor1_END);
    #else //иначе виртуальный датчик (использует микрошаги на оборот)
     Motor1_CurPos=Motor1.currentPosition();
     if(Motor1_CurPos>=Motor1_STEPSD)
     {
       Motor1.setCurrentPosition(Motor1_CurPos-Motor1_STEPSD);
       Motor1_Stoped=true;//совершон полный оборот
       Motor2.setSpeed(Motor1_Speed*2);
       Motor2.setMaxSpeed(Motor2_MaxSpeed);
     };
    #endif
    //-----------------------------------------------------------------
    if(Motor1_Stoped)//если сделан 1 оборот намоточника
    {
      if(Motor2.currentPosition()>=0) Motor2.move(Motor2_STEPSD);
      if(Motor2.currentPosition()<0) Motor2.move(-Motor2_STEPSD);
      Motor1.setCurrentPosition(0);
      Motor1_Stoped=false;
    };
    //-----------------------------------------------------------------
    if(digitalRead(FilaMinPin)==0)//пруток провис
    {
      Motor1_Kf=1.1; //set Motor1_Kf>1.0
    } else
    if(digitalRead(FilaMaxPin)==0)//пруток натянут
    {
      Motor1_Kf=0.9; //set Motor1_Kf<1.0
    } else
    {
      Motor1_Kf=1.0; //set Motor1_Kf=1.0
      //если Kf!=1.0 можно постепенно приблизиться к 1.0
      //при желании можно задействовать библиотеку PID регулировки
    }
    //если необходимо укладчик сделает заданое количество микрошогов и остановится
    Motor2.run();//эта функция даёт вращение с ускорением, но нужна дистанция перемещения, в конце автоматически останавливается
    //-----------------------------------------------------------------
  } else //выключен... (StartStopPin=0 & PrevStartStop=1)
  {
    digitalWrite(Motor1_EN, !En_ON); //намотка выключается
    #ifndef USE_PARK //парковка укладчика не используется
     digitalWrite(Motor2_EN, !En_ON);
    #else
    if(PrevStartStop)
    {
      PrevStartStop=false;
      digitalWrite(Motor2_EN, En_ON);
      Motor2.setCurrentPosition(0);
      Motor2.setSpeed(Motor2_MinSpeed);//движение к концевику с минимальной скоростью
      Motor2.setMaxSpeed(Motor2_MinSpeed);
      #ifdef INVERSE_PARK
       Motor2.move(+Motor2_STEPSD);
      #else
       Motor2.move(-Motor2_STEPSD);
      #endif
    };
    //-----------------------------------------------------------------
    if(Motor2.isRunning()) 
    {
      OutLed(100);//10Hz
      #ifdef INVERSE_PARK
       Motor2.move(+Motor2_STEPSD);//парковка в максимум
       if(digitalRead(Motor2_END_Max)!=0) Motor2.run(); else
         { Motor2.stop(); Motor2.setCurrentPosition(-1); };//добрались до максимума
      #else
       Motor2.move(-Motor2_STEPSD);//парковка в минимум
       if(digitalRead(Motor2_END_Min)!=0) Motor2.run(); else
         { Motor2.stop(); Motor2.setCurrentPosition(+1); };//добрались до минимума
      #endif
      if(!Motor2.isRunning())
      {
       Motor2.setMaxSpeed(Motor2_MaxSpeed);
       digitalWrite(Motor2_EN, !En_ON);
      }
    } else
    #endif //USE_PARK
     OutLed(500);//2Hz
  }
  //-----------------------------------------------------------------
  //парсер возможных команд в формате g-code (не реализован)
 /* "G" Codes
 *
 * G0   -> G1
 * G1   - Coordinated Movement X Y Z E
 * G28  - Home one or more axes
 * M17  - Enable/Power all stepper motors
 * M18  - Disable all stepper motors; same as M84
 * M42  - Change pin status via gcode: M42 P<pin> S<value>. LED pin assumed if P is omitted.
 * M43  - Display pin status, watch pins for changes, watch endstops & toggle LED, Z servo probe test, toggle pins
 * M92  - Set planner.axis_steps_per_mm for one or more axes.
 * M112 - Emergency stop.
 * M114 - Report current position.
 * M119 - Report endstops status.
 * M200 - Set filament diameter, D<diameter>, setting E axis units to cubic. (Use S0 to revert to linear units.)
 * M201 - Set max acceleration in units/s^2 for print moves: "M201 X<accel> Y<accel> Z<accel> E<accel>"
 * M202 - Set max acceleration in units/s^2 for travel moves: "M202 X<accel> Y<accel> Z<accel> E<accel>" ** UNUSED IN MARLIN! **
 * M203 - Set maximum feedrate: "M203 X<fr> Y<fr> Z<fr> E<fr>" in units/sec.
 * M204 - Set default acceleration in units/sec^2: P<printing> R<extruder_only> T<travel>
 * M404 - Display or set the Nominal Filament Width: "W<diameter>". (Requires FILAMENT_WIDTH_SENSOR)
 * M405 - Enable Filament Sensor flow control. "M405 D<delay_cm>". (Requires FILAMENT_WIDTH_SENSOR)
 * M406 - Disable Filament Sensor flow control. (Requires FILAMENT_WIDTH_SENSOR)
 * M407 - Display measured filament diameter in millimeters. (Requires FILAMENT_WIDTH_SENSOR)
 * M500 - Store parameters in EEPROM. (Requires EEPROM_SETTINGS)
 * M501 - Restore parameters from EEPROM. (Requires EEPROM_SETTINGS)
 * M502 - Revert to the default "factory settings". ** Does not write them to EEPROM! **
 * M503 - Print the current settings (in memory): "M503 S<verbose>". S0 specifies compact output.

 */  
  if(Serial.available())
  {
    unsigned char c;
    c=Serial.read();
    //switch (c) { case 'G': case 'M': case 'T': break; default: return; }
    
  }
}
