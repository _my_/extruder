/*  SVSoft 2018
 *  http://...
 *  Read current temperature value from port at A0-A2
 *  Read preset temperature value from port at A3
 *  Command a MOSFET controlled heating system with PID algorithm
 *  
 * For Reading a 100K thermistor.
 *============================================================
 *
 * (Gnd) ---- (100k-Resistor) -------|------- (100K-Thermistor) ---- (+5v)
 *                                   |
 *                             Analog Pin
 *============================================================
 */

//в будущем здесь нужно подключить файл с конфигом
// распиновка для UNO R3
#define TempPin1      A0       //Analog Pin thermistor 1
#define TempPin2      A1       //Analog Pin thermistor 2
#define TempPin3      A2       //Analog Pin thermistor 3
#define ThresholdPin1 A3       //Analog pin temp pot connected (для теста все на одной ноге)
#define ThresholdPin2 A3       //Analog pin temp pot connected (для теста все на одной ноге)
#define ThresholdPin3 A3       //Analog pin temp pot connected (для теста все на одной ноге) NOTE! 4 и 5 это ещё и I2C для LCD
#define HeaterPin1    3        //digital PWM pin heater 1 connected
#define HeaterPin2    5        //digital PWM pin heater 2 connected
#define HeaterPin3    6        //digital PWM pin heater 3 connected
#define Motor_En      7        //
#define Motor_Dir     8        //
#define ServoPin1     9
#define Motor_Step    10       //
#define Motor_StopL   11
#define Motor_StopR   12
   
//
#define Motor_EnON    0
#define Motor_EnOFF   1

#define BUTTON_NONE   0 // 
#define BUTTON_MENU   1 //K110
#define BUTTON_LEFT   2 //K470
#define BUTTON_DOWN   3 //1K
#define BUTTON_RIGHT  4 //4,7K
#define BUTTON_UP     5 //10K
#define BUTTON_SELECT 6



//#define USE_LCD_I2C
//#define USE_SERIAL_DBG
#define USE_MOTOR
//#define USE_SERVO
#define USE_MICROMETR

#include "D:/Clouds/MEGA/EXTRUDER_VS/PID_v1.h"
#include <math.h>
#include <Wire.h>

#ifdef USE_SERVO
 #include <servo.h>
 Servo servo1;
#endif

#ifdef USE_MOTOR
 #include <AccelStepper.h> //более продвинутая библиотека (En,Dir,Step)
 #include <MultiStepper.h> //
 //AccelStepper stepper1(AccelStepper::FULL4WIRE, pinA1, pinA2, pinB1, pinB2, false);
 AccelStepper stepper1(AccelStepper::DRIVER, Motor_Step, Motor_Dir);
 MultiStepper steppers;
#endif
#ifdef USE_LCD_I2C
 #include <LiquidCrystal_I2C.h>
 const byte LCD_ROWS = 2;
 const byte LCD_COLS = 16;
 //initialize library with LCD interface
 LiquidCrystal_I2C lcd(0x27,LCD_COLS,LCD_ROWS);
#else
 #include <LiquidCrystal.h>
 const byte LCD_ROWS = 4;
 const byte LCD_COLS = 20;
 // lcd pins
 const byte LCD_RS  = 22;
 const byte LCD_EN  = 24;
 const byte LCD_D4  = 26;
 const byte LCD_D5  = 28;
 const byte LCD_D6  = 30;
 const byte LCD_D7  = 32;
 //initialize library with LCD interface pins
 LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);  
#endif

double Output1, Output2, Output3;
double SetPoint1, SetPoint2, SetPoint3;
double ActualTemp1, ActualTemp2, ActualTemp3;
//int RawADC1, RawADC2, RawADC3;
// Timing variables used for data display
static unsigned long refreshDisplayTime;
static unsigned long reportDiaTime;
static unsigned int reportDiaInterval = 200;

//Specify the links and initial tuning parameters
//'2,5,1, DIRECT' is Kp, Ki, Kd, ControllerDirection
PID PID1(&ActualTemp1, &Output1, &SetPoint1, 2,5,1, DIRECT);
PID PID2(&ActualTemp2, &Output2, &SetPoint2, 2,5,1, DIRECT);
PID PID3(&ActualTemp3, &Output3, &SetPoint3, 2,5,1, DIRECT);

//the time we give the sensor to calibrate (10-30 secs according to the datasheet)
int calibrationTime = 2; 

double Thermister(int RawADC) {
 double Temp;
 Temp = log(((10240000/RawADC) - 10000));
 Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
 Temp = Temp - 273.15;            // Convert Kelvin to Celcius
 //Temp = (Temp * 9.0)/ 5.0 + 32.0; // Convert Celcius to Fahrenheit
 return Temp;
}

void setup() {
  pinMode(Motor_StopL, INPUT);
  pinMode(Motor_StopR, INPUT);
  digitalWrite(Motor_StopL, HIGH);
  digitalWrite(Motor_StopR, HIGH);
  pinMode(ThresholdPin1, INPUT);
  pinMode(ThresholdPin2, INPUT);
  pinMode(ThresholdPin3, INPUT);
  pinMode(HeaterPin1, OUTPUT);
  pinMode(HeaterPin2, OUTPUT);
  pinMode(HeaterPin3, OUTPUT);
  #ifdef USE_MOTOR
  pinMode(Motor_En, OUTPUT);
  digitalWrite(Motor_En, Motor_EnOFF);//снять удержание на время инициализации
  stepper1.setMaxSpeed(200*16);//3200 stepsPerSecond (т.е. 1 оборот в секунду для шаговика 1,8* с микрошагом 16)
  stepper1.setAcceleration(200.0);//stepsPerSecondSquared (ускорение)
  stepper1.setSpeed(1000.0);//1000 stepsPerSecond (скорость)
  stepper1.moveTo(5000);//��������� �������� � ���������� (��� �� ����������� �� ������� f((��)*(��� �� 1��))
  //stepper1.setMinPulseWidth(10);//default 20mks
  //stepper1.setEnablePin(Motor_En);
  //stepper1.setPinsInverted(directionInvert, stepInvert, enableInvert);//true or false
  #endif
  #ifdef USE_SERVO
   pinMode(ServoPin1, OUTPUT);
   servo1.attach(ServoPin1,544,2400);//pulse width min & max (0*..180*)
   servo1.write(90);//0-init angle -90* (default 1500, i.e. 0*)
  #endif
  #ifdef USE_MICROMETR
  
  #endif
  //-------------------------------------------------------------------------------
  Serial.begin(115200);
  Serial.print("calibrating sensor ");
  #ifdef USE_LCD_I2C // Print a message to the LCD.
   lcd.init();   // initialize the lcd
   lcd.backlight();
  #else
   lcd.begin(20, 4); //Start up LCD
  #endif
  lcd.clear();
  //give the sensor some time to calibrate
  lcd.setCursor(0,0);
  lcd.print("calibrating....");
  //-------------------------------------------------------------------------------
  for(int i = 0; i < calibrationTime; i++)
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  delay(50);
  //-------------------------------------------------------------------------------
  SetPoint1 = map(analogRead(ThresholdPin1),0,1023,0,300); //0,1023 = 0,300 > MinAnalog,MaxAnalog = MinTemp,MaxTemp
  SetPoint2 = map(analogRead(ThresholdPin2),0,1023,0,300);
  SetPoint3 = map(analogRead(ThresholdPin3),0,1023,0,300);
  ActualTemp1 = double(Thermister(analogRead(TempPin1)));
  ActualTemp2 = double(Thermister(analogRead(TempPin2)));
  ActualTemp3 = double(Thermister(analogRead(TempPin3)));
  //-------------------------------------------------------------------------------
  #ifdef USE_LCD_I2C // Print a message to the LCD.
  lcd.clear();
  lcd.setCursor(0,0); //Start at character 0 on line 0
  lcd.print("THRESHOLD  TEMP 1,2,3 ");
  lcd.setCursor(0,1);  lcd.print(SetPoint1);
  lcd.setCursor(8,1);  lcd.print(SetPoint2);
  lcd.setCursor(16,1);  lcd.print(SetPoint3);
  #endif
  #ifdef USE_SERIAL_DBG
  Serial.print(SetPoint1);  Serial.print("-");  Serial.println(ActualTemp1);
  Serial.print(SetPoint2);  Serial.print("-");  Serial.println(ActualTemp2);
  Serial.print(SetPoint3);  Serial.print("-");  Serial.println(ActualTemp3);
  Serial.println("");  // display
  #endif
  //-------------------------------------------------------------------------------
  //turn the PID on
  PID1.SetMode(AUTOMATIC);
  PID2.SetMode(AUTOMATIC);
  PID3.SetMode(AUTOMATIC);
}

int getPressedButton() //������������� ����������
{
  int buttonValue = analogRead(0); // ��������� �������� � ����������� �����
  if (buttonValue < 100) { //���� ��� ������� ������ �������� ������ 100
    return BUTTON_RIGHT;   // ������� �������� BUTTON_RIGHT
  }
  else if (buttonValue < 200) { //���� ��� ������� ������ �������� ������ 200
    return BUTTON_UP; // ������� �������� BUTTON_UP
  }
  else if (buttonValue < 400){ //���� ��� ������� ������ �������� ������ 400
    return BUTTON_DOWN; // ������� �������� BUTTON_DOWN
  }
  else if (buttonValue < 600){ //���� ��� ������� ������ �������� ������ 600
    return BUTTON_LEFT; // ������� �������� BUTTON_LEFT
  }
  else if (buttonValue < 800){ //���� ��� ������� ������ �������� ������ 800
    return BUTTON_SELECT; // ������� �������� BUTTON_SELECT
  }
  return BUTTON_NONE; //�����, ������� �������� BUTTON_NONE
}

unsigned long tim1sec;
float step_speed;

void loop() {
  static unsigned long now = millis();
  SetPoint1 = map(analogRead(ThresholdPin1),0,1023,0,300); //чтение опорной температуры с переменника
  SetPoint2 = map(analogRead(ThresholdPin2),0,1023,0,300); //
  SetPoint3 = map(analogRead(ThresholdPin3),0,1023,0,300); //
  ActualTemp1 = double(Thermister(analogRead(TempPin1)));  //чтение актуальной температуры с термистора
  ActualTemp2 = double(Thermister(analogRead(TempPin2)));  //
  ActualTemp3 = double(Thermister(analogRead(TempPin3)));  //
  //-------------------------------------------------------------------------------
//  if((SetPoint1-ActualTemp1)<20)
//  {
  //PID1.SetTunings(Kp, Ki, Kd);
//  PID1.Compute(); //подсчёт параметров ПИД регулировки
//  PID2.Compute();
//  PID3.Compute();
//  } else Output1=1;
  analogWrite(HeaterPin1,Output1); //ШИМ регулировка температуры
  analogWrite(HeaterPin2,Output2); //
  analogWrite(HeaterPin3,Output3); //
  //-------------------------------------------------------------------------------
  #ifdef USE_MOTOR
  //удержание и движение только если достигнута заданная температура +/- 1 градус
  //if(abs(ActualTemp1-SetPoint1)<=1)
  {
   digitalWrite(Motor_En, Motor_EnON);
   if (now - tim1sec > 50){ // �������� ���������� ��������
    tim1sec = now;
    step_speed = stepper1.speed();
    //if (step_speed>100)
    // stepper1.setSpeed(step_speed-10);
    #ifdef USE_SERVO
     servo1.write(servo1.read()+1);
    #endif
   }; 
   //stepper1.setSpeed(1000);//stepsPerSecond (если нужно изменить скорость)
   //stepper1.moveTo(150);//переместиться в новую позицию, или
   //stepper1.move(+100);//переместиться в новую позицию (альтернатива предыдущей функции)
   //if( ��� ��������� �������� - ������)
   if (digitalRead(Motor_StopR)==0) { stepper1.stop(); stepper1.setSpeed(0); stepper1.moveTo(0000); }; //����� ��������
   if (digitalRead(Motor_StopL)==0) { stepper1.stop(); stepper1.setSpeed(0); stepper1.moveTo(5000); }; //������ ��������
   //Serial.println(stepper1.targetPosition());
   Serial.println(stepper1.distanceToGo());
   stepper1.run();//�������� � ���������� � ����������� �� ��������� ���������
   //stepper1.runSpeed();//�������� � �������� ���������
  } 
  #endif
  //-------------------------------------------------------------------------------
  #ifdef USE_MICROMETR
  
  #endif
  //-------------------------------------------------------------------------------  
  if (now >= refreshDisplayTime)
  {
  refreshDisplayTime = now + 1000L;  
  //#ifdef USE_LCD_I2C // Print a message to the LCD.
  lcd.setCursor(0,1);  lcd.print("                ");//затираем предыдущее, это быстрее чем очищать всё
  lcd.setCursor(0,1);  lcd.print(ActualTemp1);
  lcd.setCursor(5,1);  lcd.print(ActualTemp2);
  lcd.setCursor(10,1);  lcd.print(ActualTemp3);
  //#endif
  }
  //-------------------------------------------------------------------------------
  if (now >= reportDiaTime)
  {
  reportDiaTime = now + reportDiaInterval;
  #ifdef USE_SERIAL_DBG
  Serial.println(Output1);
  Serial.print(SetPoint1);  Serial.print("-");  Serial.println(ActualTemp1);
  Serial.print(SetPoint2);  Serial.print("-");  Serial.println(ActualTemp2);
  Serial.print(SetPoint3);  Serial.print("-");  Serial.println(ActualTemp3);
  Serial.println("");  // display
  #endif
  } 

}

